
package action;

import beans.User;
import common.AuthenticationTool;
import common.JsonMsg;
import org.apache.commons.lang3.StringUtils;
import oschina.Oauth2Api;
import oschina.UserApi;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * 获取认证action
 * @author oscfox
 *
 */
@WebServlet("/Oauth2Action")
public class Oauth2Action extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		
		if(StringUtils.isBlank(code)){//授权码获取失败
			JsonMsg.json_out(JsonMsg.jsonError("授权码获取失败"),response);
			return;
		}
		
		//根据oschina authorization_code 回调的code请求 access_token
		String access_token= Oauth2Api.getAccess_token(code);
		if(StringUtils.isBlank(access_token)){//授权码获取失败
			JsonMsg.json_out(JsonMsg.jsonError("access_token获取失败"),response);
			return;
		}
		
		//根据access_token 获取User
		User user = UserApi.getUser(access_token);
		if(null == user || !StringUtils.isNumeric(user.getId())){
			JsonMsg.json_out(JsonMsg.jsonError("user获取失败"),response);
			return;
		}

        AuthenticationTool.ME.putUser(Long.parseLong(user.getId()), access_token);
		
        Cookie u = new Cookie("user",user.getId()) ;
        int maxAge = 60*10*6;//设置最长的Cookie时间为60分钟（1个小时）
		u.setMaxAge(maxAge) ;
        u.setPath("/") ;
        String userhref = user.getUrl();
        Cookie linkcookie = new Cookie("href",userhref) ;
        linkcookie.setMaxAge(maxAge) ;    
        linkcookie.setPath("/") ;
        Cookie username = new Cookie("username",URLEncoder.encode(user.getName(),"UTF-8")) ;
        username.setMaxAge(maxAge) ;    
        username.setPath("/") ;
        response.addCookie(u) ;
		response.sendRedirect("/index.html");
		
	}
}
